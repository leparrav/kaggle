# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <markdowncell>

# # Goal of this Notebook:
# 
# Analyse the <a href="http://www.kaggle.com/c/titanic-gettingStarted">Titanic set</a> project from the Kaggle, we will jump start with the use of <a href="https://github.com/agconti/kaggle-titanic">AGconti</a> already build python notebook, and improve its score to get atleast an 80%!.

# <markdowncell>

# #### First off we have to take care of imports:

# <codecell>

import re
import pandas as pd
import numpy as np
import statsmodels.api as sm

from statsmodels.nonparametric import kde, smoothers_lowess
from pandas import Series,DataFrame
from patsy import dmatrices
from sklearn import datasets, svm
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
# kaggleaux is a module that contains auxiliary functions for kaggle competitions. 
# the module is avaliable at github.com/agconti/AGC_KaggleAux
import sys
sys.path.append("/home/luis/Documentos/Kaggle/AGC_KaggleAux")
import kaggleaux as ka

# <markdowncell>

# ## Data Handling
# #### Let's read our data in using pandas:

# <codecell>

def getTitle(name_series, b):
    titles = ['Mr.','Master.','Miss.','Mrs.','Dr.','Rev.','Major.','Sir.','Capt.', 'Countess','Col.', 'Don.']
    size = len(name_series)
    for i in range(size):
        words = name_series[i].split()
        for word in words:
            if word in titles: # If word is in title add it to the list
                b.append(word[0:-1])
                break
        if i >= len(b):     # If after checking all words I didn't find it in list add empty
            b.append("")

	


df = pd.read_csv("../../Data/train.csv")
b = []
# getTitle(df['Name'],b)
# df['Titles'] = b
pattern = "(\s(Mr)?(Miss)?(Mrs)?(Dr)?(Rev)?(Major)?(Sir)?(Capt)?(Countess)?(Col)?(Don)?)"
#pattern = "(\s[MDRSC][aeori][svjrpul]?[tson]?[etr]?[rs]?(s)?)" # Either pattern would work
#pattern = "(\s(Mrs)?(Master)?)(Miss)?(Dr)?(Rev)?(Major)?(Sir)?(Capt)?(Countess)?(Col)?(Don)?(Mr)?(Ms)?)"
df['Title'] = df['Name'].str.extract(pattern)[0]
print df.head(3)






# df_output.to_csv("genderonly.csv",index=False)

# <markdowncell>

# Your submission scored 0.76555!. With only using gender information

